﻿Public Class empleado

    Private Sub EmpleadoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles EmpleadoBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.EmpleadoBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Papeleria4DataSet)

    End Sub

    Private Sub empleado_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Papeleria4DataSet.empleado' Puede moverla o quitarla según sea necesario.
        Me.EmpleadoTableAdapter.Fill(Me.Papeleria4DataSet.empleado)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.EmpleadoTableAdapter.Insert(Id_empleadoTextBox.Text, NombreTextBox.Text, ApellidoTextBox.Text, DireccionTextBox.Text, TelefonoTextBox.Text)
        Me.EmpleadoTableAdapter.Fill(Me.Papeleria4DataSet.empleado)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Hide()
        principal.Show()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        End

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.EmpleadoTableAdapter.Delete(Id_empleadoTextBox.Text, NombreTextBox.Text, ApellidoTextBox.Text, DireccionTextBox.Text, TelefonoTextBox.Text)
        Me.EmpleadoTableAdapter.Fill(Me.Papeleria4DataSet.empleado)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.EmpleadoTableAdapter.FillBy(Me.Papeleria4DataSet.empleado, TextBox1.Text)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Me.EmpleadoTableAdapter.FillBy1(Me.Papeleria4DataSet.empleado, TextBox2.Text)

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Id_empleadoTextBox.Text = ""
        NombreTextBox.Text = ""
        ApellidoTextBox.Text = ""
        DireccionTextBox.Text = ""
        TelefonoTextBox.Text = ""
        TextBox1.Text = ""
        TextBox2.Text = ""
        Id_empleadoTextBox.Focus()






    End Sub
End Class