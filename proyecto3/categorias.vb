﻿Public Class categorias

    Private Sub CategoriasBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs) Handles CategoriasBindingNavigatorSaveItem.Click
        Me.Validate()
        Me.CategoriasBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Papeleria4DataSet)

    End Sub

    Private Sub categorias_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Papeleria4DataSet.categorias' Puede moverla o quitarla según sea necesario.
        Me.CategoriasTableAdapter.Fill(Me.Papeleria4DataSet.categorias)

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.Hide()
        principal.Show()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        End

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.CategoriasTableAdapter.Insert(Id_categoriaTextBox.Text, Nombre_catTextBox.Text)

        Me.CategoriasTableAdapter.Fill(Me.Papeleria4DataSet.categorias)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.CategoriasTableAdapter.Delete(Id_categoriaTextBox.Text, Nombre_catTextBox.Text)

        Me.CategoriasTableAdapter.Fill(Me.Papeleria4DataSet.categorias)
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Me.CategoriasTableAdapter.FillBy(Me.Papeleria4DataSet.categorias, TextBox1.Text)

    End Sub
End Class