﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class art_vendidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Id_articulosvLabel As System.Windows.Forms.Label
        Dim Id_articulosLabel As System.Windows.Forms.Label
        Dim Id_ventasLabel As System.Windows.Forms.Label
        Dim Cant_vendLabel As System.Windows.Forms.Label
        Dim PrecioLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(art_vendidos))
        Me.Papeleria4DataSet = New proyecto3.papeleria4DataSet()
        Me.Art_vendidosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Art_vendidosTableAdapter = New proyecto3.papeleria4DataSetTableAdapters.art_vendidosTableAdapter()
        Me.TableAdapterManager = New proyecto3.papeleria4DataSetTableAdapters.TableAdapterManager()
        Me.Art_vendidosBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Art_vendidosBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Art_vendidosDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Id_articulosvTextBox = New System.Windows.Forms.TextBox()
        Me.Id_articulosTextBox = New System.Windows.Forms.TextBox()
        Me.Id_ventasTextBox = New System.Windows.Forms.TextBox()
        Me.Cant_vendTextBox = New System.Windows.Forms.TextBox()
        Me.PrecioTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Id_articulosvLabel = New System.Windows.Forms.Label()
        Id_articulosLabel = New System.Windows.Forms.Label()
        Id_ventasLabel = New System.Windows.Forms.Label()
        Cant_vendLabel = New System.Windows.Forms.Label()
        PrecioLabel = New System.Windows.Forms.Label()
        CType(Me.Papeleria4DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Art_vendidosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Art_vendidosBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Art_vendidosBindingNavigator.SuspendLayout()
        CType(Me.Art_vendidosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Id_articulosvLabel
        '
        Id_articulosvLabel.AutoSize = True
        Id_articulosvLabel.Location = New System.Drawing.Point(59, 108)
        Id_articulosvLabel.Name = "Id_articulosvLabel"
        Id_articulosvLabel.Size = New System.Drawing.Size(66, 13)
        Id_articulosvLabel.TabIndex = 2
        Id_articulosvLabel.Text = "id articulosv:"
        '
        'Id_articulosLabel
        '
        Id_articulosLabel.AutoSize = True
        Id_articulosLabel.Location = New System.Drawing.Point(65, 136)
        Id_articulosLabel.Name = "Id_articulosLabel"
        Id_articulosLabel.Size = New System.Drawing.Size(60, 13)
        Id_articulosLabel.TabIndex = 4
        Id_articulosLabel.Text = "id articulos:"
        '
        'Id_ventasLabel
        '
        Id_ventasLabel.AutoSize = True
        Id_ventasLabel.Location = New System.Drawing.Point(72, 162)
        Id_ventasLabel.Name = "Id_ventasLabel"
        Id_ventasLabel.Size = New System.Drawing.Size(53, 13)
        Id_ventasLabel.TabIndex = 6
        Id_ventasLabel.Text = "id ventas:"
        '
        'Cant_vendLabel
        '
        Cant_vendLabel.AutoSize = True
        Cant_vendLabel.Location = New System.Drawing.Point(67, 182)
        Cant_vendLabel.Name = "Cant_vendLabel"
        Cant_vendLabel.Size = New System.Drawing.Size(58, 13)
        Cant_vendLabel.TabIndex = 8
        Cant_vendLabel.Text = "cant vend:"
        '
        'PrecioLabel
        '
        PrecioLabel.AutoSize = True
        PrecioLabel.Location = New System.Drawing.Point(86, 208)
        PrecioLabel.Name = "PrecioLabel"
        PrecioLabel.Size = New System.Drawing.Size(39, 13)
        PrecioLabel.TabIndex = 10
        PrecioLabel.Text = "precio:"
        '
        'Papeleria4DataSet
        '
        Me.Papeleria4DataSet.DataSetName = "papeleria4DataSet"
        Me.Papeleria4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Art_vendidosBindingSource
        '
        Me.Art_vendidosBindingSource.DataMember = "art_vendidos"
        Me.Art_vendidosBindingSource.DataSource = Me.Papeleria4DataSet
        '
        'Art_vendidosTableAdapter
        '
        Me.Art_vendidosTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.art_vendidosTableAdapter = Me.Art_vendidosTableAdapter
        Me.TableAdapterManager.articulosTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.categoriasTableAdapter = Nothing
        Me.TableAdapterManager.comprasTableAdapter = Nothing
        Me.TableAdapterManager.empleadoTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = proyecto3.papeleria4DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.usuarioTableAdapter = Nothing
        Me.TableAdapterManager.ventasTableAdapter = Nothing
        '
        'Art_vendidosBindingNavigator
        '
        Me.Art_vendidosBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.Art_vendidosBindingNavigator.BindingSource = Me.Art_vendidosBindingSource
        Me.Art_vendidosBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.Art_vendidosBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.Art_vendidosBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.Art_vendidosBindingNavigatorSaveItem})
        Me.Art_vendidosBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.Art_vendidosBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.Art_vendidosBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.Art_vendidosBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.Art_vendidosBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.Art_vendidosBindingNavigator.Name = "Art_vendidosBindingNavigator"
        Me.Art_vendidosBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.Art_vendidosBindingNavigator.Size = New System.Drawing.Size(886, 25)
        Me.Art_vendidosBindingNavigator.TabIndex = 0
        Me.Art_vendidosBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Agregar nuevo"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(37, 22)
        Me.BindingNavigatorCountItem.Text = "de {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Eliminar"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Art_vendidosBindingNavigatorSaveItem
        '
        Me.Art_vendidosBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Art_vendidosBindingNavigatorSaveItem.Image = CType(resources.GetObject("Art_vendidosBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Art_vendidosBindingNavigatorSaveItem.Name = "Art_vendidosBindingNavigatorSaveItem"
        Me.Art_vendidosBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.Art_vendidosBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Art_vendidosDataGridView
        '
        Me.Art_vendidosDataGridView.AutoGenerateColumns = False
        Me.Art_vendidosDataGridView.BackgroundColor = System.Drawing.SystemColors.ButtonFace
        Me.Art_vendidosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Art_vendidosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5})
        Me.Art_vendidosDataGridView.DataSource = Me.Art_vendidosBindingSource
        Me.Art_vendidosDataGridView.Location = New System.Drawing.Point(247, 54)
        Me.Art_vendidosDataGridView.Name = "Art_vendidosDataGridView"
        Me.Art_vendidosDataGridView.Size = New System.Drawing.Size(542, 220)
        Me.Art_vendidosDataGridView.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "id_articulosv"
        Me.DataGridViewTextBoxColumn1.HeaderText = "id_articulosv"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "id_articulos"
        Me.DataGridViewTextBoxColumn2.HeaderText = "id_articulos"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "id_ventas"
        Me.DataGridViewTextBoxColumn3.HeaderText = "id_ventas"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "cant_vend"
        Me.DataGridViewTextBoxColumn4.HeaderText = "cant_vend"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "precio"
        Me.DataGridViewTextBoxColumn5.HeaderText = "precio"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        '
        'Id_articulosvTextBox
        '
        Me.Id_articulosvTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Art_vendidosBindingSource, "id_articulosv", True))
        Me.Id_articulosvTextBox.Location = New System.Drawing.Point(131, 105)
        Me.Id_articulosvTextBox.Name = "Id_articulosvTextBox"
        Me.Id_articulosvTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Id_articulosvTextBox.TabIndex = 3
        '
        'Id_articulosTextBox
        '
        Me.Id_articulosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Art_vendidosBindingSource, "id_articulos", True))
        Me.Id_articulosTextBox.Location = New System.Drawing.Point(131, 133)
        Me.Id_articulosTextBox.Name = "Id_articulosTextBox"
        Me.Id_articulosTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Id_articulosTextBox.TabIndex = 5
        '
        'Id_ventasTextBox
        '
        Me.Id_ventasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Art_vendidosBindingSource, "id_ventas", True))
        Me.Id_ventasTextBox.Location = New System.Drawing.Point(131, 159)
        Me.Id_ventasTextBox.Name = "Id_ventasTextBox"
        Me.Id_ventasTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Id_ventasTextBox.TabIndex = 7
        '
        'Cant_vendTextBox
        '
        Me.Cant_vendTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Art_vendidosBindingSource, "cant_vend", True))
        Me.Cant_vendTextBox.Location = New System.Drawing.Point(131, 179)
        Me.Cant_vendTextBox.Name = "Cant_vendTextBox"
        Me.Cant_vendTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Cant_vendTextBox.TabIndex = 9
        '
        'PrecioTextBox
        '
        Me.PrecioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Art_vendidosBindingSource, "precio", True))
        Me.PrecioTextBox.Location = New System.Drawing.Point(131, 205)
        Me.PrecioTextBox.Name = "PrecioTextBox"
        Me.PrecioTextBox.Size = New System.Drawing.Size(100, 20)
        Me.PrecioTextBox.TabIndex = 11
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button5.Location = New System.Drawing.Point(640, 331)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 22
        Me.Button5.Text = "regresar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button3.Location = New System.Drawing.Point(721, 331)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 20
        Me.Button3.Text = "salir"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Button2.Location = New System.Drawing.Point(349, 298)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 19
        Me.Button2.Text = "eliminar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(268, 298)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 18
        Me.Button1.Text = "insertar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.TextBox1.Location = New System.Drawing.Point(509, 300)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 24
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button7.Location = New System.Drawing.Point(428, 298)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 23
        Me.Button7.Text = "buscar ID"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(509, 333)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 26
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button6.Location = New System.Drawing.Point(428, 331)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 25
        Me.Button6.Text = "nombre"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'art_vendidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.proyecto3.My.Resources.Resources._602_l1
        Me.ClientSize = New System.Drawing.Size(886, 391)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(PrecioLabel)
        Me.Controls.Add(Me.PrecioTextBox)
        Me.Controls.Add(Cant_vendLabel)
        Me.Controls.Add(Me.Cant_vendTextBox)
        Me.Controls.Add(Id_ventasLabel)
        Me.Controls.Add(Me.Id_ventasTextBox)
        Me.Controls.Add(Id_articulosLabel)
        Me.Controls.Add(Me.Id_articulosTextBox)
        Me.Controls.Add(Id_articulosvLabel)
        Me.Controls.Add(Me.Id_articulosvTextBox)
        Me.Controls.Add(Me.Art_vendidosDataGridView)
        Me.Controls.Add(Me.Art_vendidosBindingNavigator)
        Me.Name = "art_vendidos"
        Me.Text = "art_vendidos"
        CType(Me.Papeleria4DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Art_vendidosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Art_vendidosBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Art_vendidosBindingNavigator.ResumeLayout(False)
        Me.Art_vendidosBindingNavigator.PerformLayout()
        CType(Me.Art_vendidosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Papeleria4DataSet As proyecto3.papeleria4DataSet
    Friend WithEvents Art_vendidosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Art_vendidosTableAdapter As proyecto3.papeleria4DataSetTableAdapters.art_vendidosTableAdapter
    Friend WithEvents TableAdapterManager As proyecto3.papeleria4DataSetTableAdapters.TableAdapterManager
    Friend WithEvents Art_vendidosBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Art_vendidosBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Art_vendidosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Id_articulosvTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Id_articulosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Id_ventasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Cant_vendTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PrecioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
End Class
